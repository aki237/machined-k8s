# systemd-machined based kubernetes cluster

A test project to automate spinning up kubernetes clusters using
systemd-machined containers as nodes.

**State**: Nodes are running with _PID 1_. `kubeadm` stuff yet to be done.

## Instructions

### Pre-req

 - `ansible`
 - `debootstrap` - (uses debain stable to bootstrap the root fs. Debian 11 @ the time of writing this)
 - `systemd-networkd.service` is running

### Setup a Variables file

Make a `vars_test.yml` file with the following parameters defined:

```yaml
pub_key: "/path/to/id_rsa.pub"                                # key to be authorized for ssh logins
output: "/var/lib/containers/debain-11-base"                  # directory where the ro root filesystem needs to formed
overlays_path: "/home/aki237/Desktop/nspawn-cluster/overlays" # directory in which the rw overlay directories for different nodes are stored
```

Make sure to add more hosts in the `inventory.yml` and the corresponding address for the same (within the 172.17.0.1/24 subnet).
The default address group can be updated in the systemd network for the zoned bridge in the host side (See [_this_](systemd/80-container-vz.network)).
Make sure to update the network file in `/usr/lib/systemd/network/80-container-vz.network`) and reconfigure the device using `networkctl reload`.

### Stage 1 - Bootstrap a root filesystem

Run the bootstrap.yml playbook

```
ansible-playbook -i inventory.yml -e "@vars_test.yml" -K -b --user root bootstrap.yml
```

### Stage 2 - Generate overlay RW fs for each node

```
ansible-playbook -i inventory.yml -e "@vars_test.yml" -K -b --user root generate_node_overlays.yml
```

### Stage 3 - Run the node containers

```
ansible-playbook -i inventory.yml -e "@vars_test.yml" -K -b --user root start_nodes.yml
```
